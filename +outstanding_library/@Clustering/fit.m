function obj = fit(obj,Xtr,Ytr)
    %METHOD1 Summary of this method goes here
    %   Detailed explanation goes here
    [FEASIBILITY,CENTROIDS,MEMBERSHIPS,HYPERPLANES,COVARIANCES,CARDINALITIES,DBI,SILHOUETTE,MAXANFISMF] = deal(cell(obj.numClusters(end),1));
    switch obj.mode
        case 'partitional'
            for K = obj.numClusters
                outputArg = obj.fitPartitional(Xtr,Ytr,K);
                FEASIBILITY{K} = outputArg{1};
                CENTROIDS{K} = outputArg{2};
                MEMBERSHIPS{K} = outputArg{3};
                HYPERPLANES{K} = outputArg{4};
                COVARIANCES{K} = outputArg{5};
                CARDINALITIES{K} = outputArg{6};
                DBI{K} = outputArg{7};
                SILHOUETTE{K} = outputArg{8};
                MAXANFISMF{K} = outputArg{9};
            end
            % results as a tree
            for K = obj.numClusters
                FEASIBILITY(K,1:K) = FEASIBILITY{K};
                CENTROIDS(K,1:K) = CENTROIDS{K};
                MEMBERSHIPS(K,1:K) = MEMBERSHIPS{K};
                HYPERPLANES(K,1:K) = HYPERPLANES{K};
                COVARIANCES(K,1:K) = COVARIANCES{K};
                CARDINALITIES(K,1:K) = CARDINALITIES{K};
                % DBI(K,1:K) = DBI{K};
                % SILHOUETTE(K,1:K) = SILHOUETTE{K};
                MAXANFISMF(K,1:K) = MAXANFISMF{K};
            end
            DBI = DBI(:,1);
            SILHOUETTE = SILHOUETTE(:,1);
        case 'agglomerative'
            outputArg = obj.fitAgglomerative(Xtr,Ytr,max(obj.numClusters));
            FEASIBILITY = outputArg{1};
            CENTROIDS = outputArg{2};
            MEMBERSHIPS = outputArg{3};
            HYPERPLANES = outputArg{4};
            COVARIANCES = outputArg{5};
            CARDINALITIES = outputArg{6};
            DBI = outputArg{7};
            SILHOUETTE = outputArg{8};
            MAXANFISMF = outputArg{9};
        case 'divisive'
            outputArg = obj.fitDivisive(Xtr,Ytr,max(obj.numClusters));
            FEASIBILITY = outputArg{1};
            CENTROIDS = outputArg{2};
            MEMBERSHIPS = outputArg{3};
            HYPERPLANES = outputArg{4};
            COVARIANCES = outputArg{5};
            CARDINALITIES = outputArg{6};
            DBI = outputArg{7};
            SILHOUETTE = outputArg{8};
            MAXANFISMF = outputArg{9};
    end
    
    %%% Output
    obj.feasibility = FEASIBILITY;
    obj.centroids = CENTROIDS;
    obj.memberships = MEMBERSHIPS;
    obj.hyperplanes = HYPERPLANES;
    obj.covariances = COVARIANCES;
    obj.cardinalities = CARDINALITIES;
    obj.DBI = DBI;
    obj.silhouette = SILHOUETTE;
    obj.maxAnfisMF = MAXANFISMF;
end