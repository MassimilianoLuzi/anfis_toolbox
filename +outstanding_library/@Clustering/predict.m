function idx = predict(obj,X,K)
%METHOD1 Summary of this method goes here
%   Detailed explanation goes here

    thisCentres = cell2mat(obj.centroids(K,:)');

    switch  obj.inputDistance
        case 'sqEuclidean'
            D = pdist2(X, thisCentres, 'euclidean').^2;
            [~, idx]=min(D,[],2);
        case 'euclidean'
            D = pdist2(X, thisCentres, 'euclidean');
            [~, idx]=min(D,[],2);
        case 'manhattan'
            D = pdist2(X, thisCentres, 'cityblock');
            [~, idx]=min(D,[],2);
        case 'mahalanobis'
            thisCardinalities = obj.memberships(2,:);
            thisCardinalities = cellfun(@(x) length(x), thisCardinalities);
            for k = 1:K
                try
                    D(:,k) = pdist2(X,thisCentres(k,:),'mahalanobis',obj.covariances{K,k});
                catch
                    if thisCardinalities(k) == 1
                        % for singleton clusters there is no covariance
                        % hence, rollback to Euclidean distance
                        D(:,k) = pdist2(X,thisCentres(k,:),'euclidean');
                    else
                        % for badly-scaled non-singleton clusters
                        % use the modified Mahalanobis
                        % eval eigs
                        [V,W] = eig(obj.covariances{K,k},'vector');
                        W(abs(W)<eps) = 0;
                        % swap
                        [~, idx] = sort(W);
                        idx = fliplr(idx');
                        V = V(:,idx);
                        W = diag(W(idx));
                        % normalise
                        smallEig = diag(W);
                        smallEig(smallEig==0) = inf;
                        smallEig = min(smallEig);
                        for i = 1:size(W,1)
                            W(i,i) = 1/max(W(i,i),smallEig);
                        end
                        % eval dist
                        D(:,k) = sqrt(diag((X-thisCentres(k,:))*V*W*V'*(X-thisCentres(k,:))'));
                    end
                end
                [~, idx]=min(D,[],2);
            end
    end

end
