classdef Clustering
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        % Clustering Output
        feasibility
        centroids
        memberships
        hyperplanes
        covariances
        cardinalities
        DBI
        silhouette
        maxAnfisMF
        
        % user input
        representative      % 'centroid', 'medoid', 'median'
        inputDistance       % input distance: 'euclidean', 'sqEuclidean', 'manhattan', 'mahalanobis' or function handle
        numClusters         % positive scalar (Kmax) or vector (K candidates)
        numReplicates       % positive scalar
        maxIters            % positive scalar
        DaviesBouldin       % true/false (want/unwant)
        Silhouette          % true/false (want/unwant)
        mode                % 'agglomerative', 'divisive', 'partitional'
        parallel            % true/false (want/unwant)
        epsilon             % joint input-hyperplanes weight
        % automatically built
        %         joinedDistance
    end
    
    methods
        function obj = Clustering(mode,representative,distance,numClusters,numReplicates,maxIters,epsilon,DaviesBouldin,Silhouette,parallel)
            %UNTITLED Construct an instance of this class
            %   Detailed explanation goes here
            
            if strcmp(representative,'centroid')==false && strcmp(representative,'medoid')==false && strcmp(representative,'median')==false
                error('Input ''representative'' must either be ''centroid'', ''medoid'' or ''median''.');
            else
                obj.representative = representative;
            end
            
            if ischar(distance)
                if strcmp(distance,'euclidean')==false && strcmp(distance,'sqEuclidean')==false && strcmp(distance,'manhattan')==false && strcmp(distance,'mahalanobis')==false
                    error('Input (built-in) ''distance'' must either be ''euclidean'', ''sqEuclidean'' or ''manhattan'' or ''mahalanobis''.');
                end
            end
            obj.inputDistance = distance;
            
            %             if numClusters < 0
            %                 error('Input ''numClusters'' must be a positive integer scalar.');
            %             end
            %
            %             obj.numClusters = numClusters;
            
            if isscalar(numReplicates) && numReplicates>0
                obj.numReplicates = numReplicates;
            else
                error('Input ''numReplicates'' must be a positive scalar.');
            end
            
            if isscalar(maxIters) && maxIters>0
                obj.maxIters = maxIters;
            else
                error('Input ''maxIters'' must be a positive scalar.');
            end
            
            if DaviesBouldin~=1 && DaviesBouldin~=0 && DaviesBouldin~=true && DaviesBouldin~=false
                error('Input ''DaviesBouldin'' must either be true (1) or false (0).');
            else
                obj.DaviesBouldin = DaviesBouldin;
            end
            
            if Silhouette~=1 && Silhouette~=0 && Silhouette~=true && Silhouette~=false
                error('Input ''Silhouette'' must either be true (1) or false (0).');
            else
                obj.Silhouette = Silhouette;
            end
            
            if strcmp(mode,'agglomerative')==false && strcmp(mode,'divisive')==false && strcmp(mode,'partitional')==false
                error('Input ''mode'' must either be ''agglomerative'', ''divisive'' or ''partitional''.');
            else
                obj.mode = mode;
            end
            
            if parallel~=1 && parallel~=0 && parallel~=true && parallel~=false
                error('Input ''parallel'' must either be true (1) or false (0).');
            else
                obj.parallel = parallel;
            end
            
            if epsilon>1 || epsilon<0
                error('Input ''epsilon'' must be in range [0,1].');
            else
                obj.epsilon = epsilon;
            end
            
            if any(numClusters<=0)
                error('All values in ''numClusters'' must be positive.');
            end
            if sum(mod(numClusters,1))~=0
                error('All values in ''numClusters'' must be integers.');
            end
            %             if strcmp(mode,'agglomerative') || strcmp(mode,'divisive')
            %                 %                 if isscalar(numClusters)==false
            %                 %                     error('Input ''numClusters'' must be scalar for modes ''agglomerative'' and ''divisive''.');
            %                 if isequal(numClusters,1:max(numClusters))==false
            %                     error('Input ''numClusters'' must be a vector of the form [1,numClusters_max] for modes ''agglomerative'' and ''divisive''.');
            %                 else
            %                     obj.numClusters = numClusters;
            %                 end
            %             elseif strcmp(mode,'partitional')
            %                 obj.numClusters = numClusters;
            %             end
            obj.numClusters = numClusters;
            
            %             obj.epsilon = epsilon;
            %             obj.joinedDistance = @(pattern,label,centroid,hyperplane,cluster,epsilon) obj.epsilon*obj.inputDistance(pattern,centroid,cluster) + (1-obj.epsilon)*(label-dot([1 pattern],hyperplane))^2;
        end
    end
    
    methods (Access = private, Hidden = true)
        outputArg = fitPartitional(obj,Xtr,Ytr,K);
        outputArg = fitAgglomerative(obj,Xtr,Ytr,K);
        outputArg = fitDivisive(obj,Xtr,Ytr,K);
    end
end