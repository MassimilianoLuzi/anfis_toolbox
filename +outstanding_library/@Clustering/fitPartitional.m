function outputArg = fitPartitional(obj,Xtr,Ytr,K)
%%% input consistency check
[numPatterns, numFeatures] = size(Xtr);
Ytr = Ytr(:);   % make sure Ytr is a column vector

if length(Ytr)~=numPatterns
    error('Number of patterns and number of labels mismatch.');
end
if mod(K,1)~=0
    error('Parameter ''K'' must be integer.');
end
if K<=0 || K>size(unique(Xtr,'rows'),1)
    error('Parameter ''K'' must be greater than zero and at most equal to the number of distinct patterns in DATASET.');
end
if mod(obj.numReplicates,1)~=0
    error('Parameter ''numReplicates'' must be integer.');
end
if mod(obj.maxIters,1)~=0
    error('Parameter ''maxIters'' must be integer.');
end
if obj.epsilon<0 || obj.epsilon>1
    error('Parameter ''epsilon'' must be in range [0,1].');
end
if obj.parallel == false
    parforArg = 0;
else
    parforArg = Inf;
end

%%% init data structures for replicates storage
MEMBERSHIPS=cell(1,obj.numReplicates);
INERTIA=cell(1,obj.numReplicates);
CENTROIDS=cell(1,obj.numReplicates);
COVARIANCES=cell(obj.numReplicates,K);
HYPERPLANES=cell(obj.numReplicates,K);
CARDINALITIES=cell(obj.numReplicates,K);

%%% start replicates
parfor(rep = 1:obj.numReplicates,parforArg)
    %for rep = 1:obj.numReplicates
    tic;
    % select initial centroids and hyperplanes [old]
    %     hyperplanes=cell(1,K);
    %     centres = zeros(K,numFeatures);
    %     numHyperplanes = K;
    %     patternPerHyperplane = floor(numPatterns/K);
    %     idx = randperm(numHyperplanes*patternPerHyperplane);
    %     idx = reshape(idx, numHyperplanes, patternPerHyperplane);
    %     for k = 1:K
    %         patternsInCluster = Xtr(idx(k,:),:);
    %         labelsInCluster = Ytr(idx(k,:));
    %         hyperplanes{k} = pinv([ones(size(patternsInCluster,1),1) patternsInCluster])*labelsInCluster;
    %         centres(k,:) = Xtr(idx(k,1),:);
    %     end
    %     memberships = zeros(size(Xtr,1),1);
    %     for k = 1:K
    %         memberships(idx(k,:),:)=k;
    %     end
    % select initial centroids and hyperplanes [new]
    hyperplanes=cell(1,K);
    centres = randsample(1:numPatterns,K,false);
    centres = Xtr(centres,:);
    %         centres(1,:) = mean([0.05   0.05;  0.15  0.95 ],1);
    %         centres(2,:) = mean([0.23   0.2;  0.7   0.4],1);
    %         centres(3,:) = mean([0.2    0.65;  0.8   0.7 ],1);
    %         centres(4,:) = mean([0.85   0.05;  0.95  0.95 ],1);
    switch obj.inputDistance
        case {'euclidean','mahalanobis'}
            distanceMatrix = pdist2(Xtr,centres,'euclidean');
        case 'sqEuclidean'
            distanceMatrix = pdist2(Xtr,centres,'euclidean').^2;
        case 'manhattan'
            distanceMatrix = pdist2(Xtr,centres,'cityblock');
        otherwise
            distanceMatrix = zeros(size(Xtr,1),1);
            for p=1:size(Xtr,1)
                for q=1:size(centres,1)
                    distanceMatrix(p,q) = obj.distance(Xtr(p,:),centres(q,:));
                end
            end
    end
    [~, memberships] = min(distanceMatrix,[],2);
    for k = 1:K
        patternsInCluster = Xtr(memberships==k,:);
        labelsInCluster = Ytr(memberships==k);
        hyperplanes{k} = pinv([ones(size(patternsInCluster,1),1) patternsInCluster])*labelsInCluster;
    end
    % start Voronoi iterations
    for iter = 1:obj.maxIters
        % backup previous centroids for stopping criterion
        previousCentres = centres;
        % expectation (assign pattern to closest cluster)
        switch obj.inputDistance
            case 'euclidean'
                inputSpaceDist = pdist2(Xtr,centres,'euclidean');
            case 'sqEuclidean'
                inputSpaceDist = pdist2(Xtr,centres,'euclidean').^2;
            case 'manhattan'
                inputSpaceDist = pdist2(Xtr,centres,'cityblock');
            case 'mahalanobis'
                inputSpaceDist = zeros(numPatterns,K);
                for k = 1:K
                    try
                        inputSpaceDist(:,k) = pdist2(Xtr,centres(k,:),'mahalanobis',nancov(Xtr(memberships==k,:)));
                    catch
                        if size(Xtr(memberships==k,:),1)==1
                            % for singleton clusters there is no covariance
                            % hence, rollback to Euclidean distance
                            inputSpaceDist(:,k) = pdist2(Xtr,centres(k,:),'euclidean');
                        else
                            % for badly-scaled non-singleton clusters
                            % use the modified Mahalanobis
                            % eval eigs
                            [V,W] = eig(nancov(Xtr(memberships==k,:)),'vector');
                            W(abs(W)<eps) = 0;
                            % swap
                            [~, idx] = sort(W);
                            idx = fliplr(idx');
                            V = V(:,idx);
                            W = diag(W(idx));
                            % normalise
                            smallEig = diag(W);
                            smallEig(smallEig==0) = inf;
                            smallEig = min(smallEig);
                            for i = 1:size(W,1)
                                W(i,i) = 1/max(W(i,i),smallEig);
                            end
                            % eval dist
                            inputSpaceDist(:,k) = sqrt(diag((Xtr-centres(k,:))*V*W*V'*(Xtr-centres(k,:))'));
                        end
                    end
                    %                     try
                    %                         inputSpaceDist(:,k) = pdist2(Xtr,centres(k,:),'mahalanobis',nancov(Xtr(memberships==k,:)));
                    %                     catch
                    %                         %                         inputSpaceDist(:,k) = pdist2(Xtr,centres(k,:),'euclidean');
                    %                         inputSpaceDist(:,k) = sqrt(diag((Xtr-centres(k,:))*pinv(nancov(Xtr(memberships==k,:)))*(Xtr-centres(k,:))'));
                    %                     end
                end
            otherwise
                inputSpaceDist = zeros(numPatterns,K);
                for p=1:numPatterns
                    for k=1:K
                        inputSpaceDist(p,k) = obj.distance(Xtr(p,:),centres(k,:));
                    end
                end
        end
        hyperplanesSpaceDist = (Ytr - [ones(numPatterns,1) Xtr] * cell2mat(hyperplanes)).^2;
        distanceMatrix = obj.epsilon*inputSpaceDist + (1-obj.epsilon)*hyperplanesSpaceDist;
        [pointToCenterDist, memberships] = min(distanceMatrix,[],2);
        % if there are some empty clusters, initialize new cluster(s) with farthest point(s)
        if length(unique(memberships))~=K
            %                warning('Empty cluster(s)! Reassigning...');
            emptyClustersID=setdiff(1:K,unique(memberships));
            while ~isempty(emptyClustersID)
                [~,farthestPoint] = max(pointToCenterDist);             % select farthest point from its representative
                memberships(farthestPoint) = emptyClustersID(1);        % change membership with empty cluster
                centres(emptyClustersID(1),:) = Xtr(farthestPoint,:);   % change representative with pattern itself
                pointToCenterDist(farthestPoint) = 0;                   % mark the distance with itself to 0
                emptyClustersID=setdiff(1:K,unique(memberships));       % update empty cluster IDs after reassignment
            end
        end
        % maximization (update cluster representatives)
        for k = 1:K
            patternsInCluster = Xtr(memberships==k,:);
            labelsInCluster = Ytr(memberships==k);
            % update representatives
            switch obj.representative
                case 'medoid'
                    switch obj.inputDistance
                        case 'euclidean'
                            distanceMatrix = pdist2(patternsInCluster,patternsInCluster,'euclidean');
                        case 'mahalanobis'
                            %                             try
                            %                                 distanceMatrix = pdist2(patternsInCluster,patternsInCluster,'mahalanobis', nancov(patternsInCluster));
                            %                             catch
                            %                                 distanceMatrix=[];
                            %                                 for i = 1:size(patternsInCluster,1)
                            %                                     distanceMatrix(i,:) = sqrt(diag((patternsInCluster(i,:)-patternsInCluster)*pinv(nancov(patternsInCluster))*(patternsInCluster(i,:)-patternsInCluster)'));
                            %                                 end
                            %                                 %                                 distanceMatrix = pdist2(patternsInCluster,patternsInCluster,'mahalanobis', nancov(Xtr));
                            %                             end
                            try
                                distanceMatrix = pdist2(patternsInCluster,patternsInCluster,'mahalanobis', nancov(patternsInCluster));
                            catch
                                % eval eigs
                                [V,W] = eig(nancov(patternsInCluster),'vector');
                                W(abs(W)<eps) = 0;
                                % swap
                                [~, idx] = sort(W);
                                idx = fliplr(idx');
                                V = V(:,idx);
                                W = diag(W(idx));
                                % normalise
                                smallEig = diag(W);
                                smallEig(smallEig==0) = inf;
                                smallEig = min(smallEig);
                                for i = 1:size(W,1)
                                    W(i,i) = 1/max(W(i,i),smallEig);
                                end
                                % eval dist
                                distanceMatrix=[];
                                for i = 1:size(patternsInCluster,1)
                                    distanceMatrix(i,:)= sqrt(diag((patternsInCluster(i,:)-patternsInCluster)*V*W*V'*(patternsInCluster-patternsInCluster(i,:))'));
                                end
                            end
                        case 'sqEuclidean'
                            distanceMatrix = pdist2(patternsInCluster,patternsInCluster,'euclidean').^2;
                        case 'manhattan'
                            distanceMatrix = pdist2(patternsInCluster,patternsInCluster,'cityblock');
                        otherwise
                            distanceMatrix = zeros(length(labelsInCluster));
                            for p=1:length(labelsInCluster)
                                for q=1:length(labelsInCluster)
                                    distanceMatrix(p,q) = obj.distance(patternsInCluster(p,:),patternsInCluster(q,:));
                                end
                            end
                    end
                    [~,medoidID] = min(sum(distanceMatrix,1));
                    centres(k,:) = patternsInCluster(medoidID,:);
                case 'centroid'
                    centres(k,:) = mean(patternsInCluster,1);
                case 'median'
                    centres(k,:) = median(patternsInCluster,1);
            end
            % update hyperplane
            hyperplanes{k} = pinv([ones(size(patternsInCluster,1),1) patternsInCluster])*labelsInCluster;
        end
        % stopping criterion
        if isequal(previousCentres,centres) || iter==obj.maxIters
            COVARIANCES(rep,:)=arrayfun(@(k) cov(Xtr(memberships==k,:)),1:K,'UniformOutput',false);
            MEMBERSHIPS{rep}=memberships;
            CENTROIDS{rep}=centres;
            HYPERPLANES(rep,:)=hyperplanes;
            % check whether all clusters contain a number of patterns at
            % least equal to the number of features plus one: if so, we consider the
            % hyperplane (and the cluster) valid, otherwise we mark the
            % WCSoD as infinite
            [cardinalities, ~] = hist(memberships,unique(memberships));
            CARDINALITIES(rep,:) = num2cell(cardinalities);
            if sum(cardinalities<=numFeatures)>0
                INERTIA{rep}=Inf;
            else
                INERTIA{rep}=sum(pointToCenterDist);
            end
            % summary
            fprintf(['Replicate %d out of %d for k = %d:\n' ...
                '\tConvergence reached after %d iterations\n' ...
                '\tObjective function: %f\n' ...
                '\tTime elapsed: %.2f sec\n\n'] ...
                ,rep,obj.numReplicates,K,iter,INERTIA{rep},toc);
            break
        end
    end
end

% return lowest WCSoD solution amongst replicates
[~,bestSolution] = min(cell2mat(INERTIA));
fprintf('BUTTA STEF, %f \n', min(cell2mat(INERTIA)))
CENTROIDS = CENTROIDS{bestSolution};
MEMBERSHIPS = MEMBERSHIPS{bestSolution};
HYPERPLANES = HYPERPLANES(bestSolution,:);
COVARIANCES = COVARIANCES(bestSolution,:);
CARDINALITIES = CARDINALITIES(bestSolution,:);
% for the sake of ease, convert CENTROIDS to cell array (as HYPERPLANE and COVARIANCES)
CENTROIDS = mat2cell(CENTROIDS,ones(1,K),numFeatures)';

%%% Other output parameters
CLUSTER_PATTERNS = arrayfun(@(k) Xtr(MEMBERSHIPS==k,:),1:K,'UniformOutput',false);
% Feasibility
FEASIBILITY = cellfun(@(x) x>numFeatures,CARDINALITIES,'UniformOutput',false);
% Max of Membership Function
MAXANFISMF = cellfun(@(x) (2*pi)^(-numFeatures/2) * det(x)^(-1/2), COVARIANCES, 'UniformOutput',false);
% Reformat MEMBERSHIPS as cell of indices
tmp_memberships = MEMBERSHIPS;
MEMBERSHIPS = arrayfun(@(k) find(tmp_memberships==k),1:K,'UniformOutput',false);

clear tmp_memberships;

%%% Davies-Bouldin Index
if obj.DaviesBouldin == true
    if K==1
        DBI = NaN;  % DBI is not defined for k = 1 cluster
    else
        tic; fprintf('\nEvaluating Davies-Bouldin Index...');
        s = zeros(1,K);
        Rij = zeros(K);
        % evaluating pairwise distances between clusters (representatives)
        representatives = CENTROIDS(cellfun(@(x) ~isempty(x),CENTROIDS));
        representatives = reshape(cell2mat(representatives),numFeatures,[])';
        %         switch algorithm
        %             case 'kmeans'
        %                 d = pdist2(representatives_asmatrix,representatives_asmatrix,'euclidean').^2;
        %             case 'kmedians'
        %                 d = pdist2(representatives_asmatrix,representatives_asmatrix,'cityblock');
        %             case 'kmedoids'
        %                 d = pdist2(representatives_asmatrix,representatives_asmatrix,'euclidean');
        %         end
        switch obj.inputDistance
            case {'euclidean', 'mahalanobis'}
                d = pdist2(representatives,representatives,'euclidean');
            case 'sqEuclidean'
                d = pdist2(representatives,representatives,'euclidean').^2;
            case 'manhattan'
                d = pdist2(representatives,representatives,'cityblock');
            otherwise
                d = zeros(size(representatives,1));
                for p=1:size(representatives,1)
                    for q=1:size(representatives,1)
                        d(p,q) = obj.distance(representatives(p,:),representatives(q,:));
                    end
                end
        end
        % evaluating intra-cluster average distance
        for i=1:K
            %             patternsInCluster = CLUSTER_PATTERNS{k,i};
            %             switch algorithm
            %                 case 'kmeans'
            %                     s(i) = mean(pdist2(patternsInCluster,representatives{i},'euclidean').^2);
            %                 case 'kmedians'
            %                     s(i) = mean(pdist2(patternsInCluster,representatives{i},'cityblock'));
            %                 case 'kmedoids'
            %                     s(i) = mean(pdist2(patternsInCluster,representatives{i},'mahalanobis',COVARIANCES{k,i}));
            %             end
            switch obj.inputDistance
                case 'euclidean'
                    s(i) = mean(pdist2(CLUSTER_PATTERNS{i},representatives(i,:),'euclidean'));
                case 'mahalanobis'
                    s(i) = mean(pdist2(CLUSTER_PATTERNS{i},representatives(i,:),'mahalanobis',COVARIANCES{k,i}));
                case 'sqEuclidean'
                    s(i) = mean(pdist2(CLUSTER_PATTERNS{i},representatives(i,:),'euclidean').^2);
                case 'manhattan'
                    s(i) = mean(pdist2(CLUSTER_PATTERNS{i},representatives(i,:),'cityblock'));
                otherwise
                    tmp = zeros(size(CLUSTER_PATTERNS{i},1),1);
                    for p=1:length(tmp)
                        tmp(p) = obj.distance(representatives(i,:),CLUSTER_PATTERNS{i}(p,:));
                    end
                    s(i) = mean(tmp);
            end
        end
        % evaluating cross-variance between clusters
        for i=1:K
            for j=1:K
                Rij(i,j)=(s(i)+s(j))/(d(i,j));
            end
        end
        Rij(1:K+1:end) = -1; % place -1 on diagonal to trick the max operator
        R = max(Rij,[],2);
        DBI = sum(R)/K;
    end
    fprintf('done!\t\t\t[Time elapsed: %.2f]\n',toc);
elseif obj.DaviesBouldin == false
    DBI = NaN; % NaN as a dummy value if user does not want DBI
end

%%% Silhouette
if obj.Silhouette == true
    fprintf('Evaluating the Silhouette...'); tic;
    %     for k = 2:K
    [a,b,s]=deal(zeros(numPatterns,1));
    parfor(p=1:numPatterns,parforArg) % can be done in parallel
        clusterID = cellfun(@(x) find(x==p),MEMBERSHIPS,'UniformOutput',false);
        clusterID = find(cellfun(@(x) ~isempty(x),clusterID));
        clusterMembersID = MEMBERSHIPS{clusterID};

        %         a_tmp = zeros(length(clusterMembersID),1);
        %         for i = 1:length(clusterMembersID)
        %             a_tmp(i) = obj.joinedDistance(Xtr(p,:), Xtr(clusterMembersID(i),:), ) %pattern,label,centroid,hyperplane,cluster,epsilon
        %         end

        switch obj.inputDistance
            case 'euclidean'
                a_tmp = pdist2(Xtr(p,:),Xtr(clusterMembersID,:),'euclidean');
            case 'mahalanobis'
                a_tmp = pdist2(Xtr(p,:),Xtr(clusterMembersID,:),'mahalanobis',COVARIANCES{clusterID});
            case 'sqEuclidean'
                a_tmp = pdist2(Xtr(p,:),Xtr(clusterMembersID,:),'euclidean').^2;
            case 'manhattan'
                a_tmp = pdist2(Xtr(p,:),Xtr(clusterMembersID,:),'cityblock');
            otherwise
                a_tmp = zeros(length(clusterMembersID),1);
                for q=1:length(a_tmp)
                    a_tmp(q) = obj.distance(Xtr(p,:),Xtr(clusterMembersID(q),:));
                end
        end
        a(p) = sum(a_tmp) / (length(clusterMembersID)-1);

        b_tmp = [];
        for i = 1:K
            if i~=clusterID
                notClusterMembersID = MEMBERSHIPS{i};
                switch obj.inputDistance
                    case 'euclidean'
                        b_tmp = [b_tmp mean(pdist2(Xtr(p,:),Xtr(notClusterMembersID,:),'euclidean'))];
                    case 'mahalanobis'
                        b_tmp = [b_tmp mean(pdist2(Xtr(p,:),Xtr(notClusterMembersID,:),'mahalanobis',COVARIANCES{i}))];
                    case 'sqEuclidean'
                        b_tmp = [b_tmp mean(pdist2(Xtr(p,:),Xtr(notClusterMembersID,:),'euclidean').^2)];
                    case 'manhattan'
                        b_tmp = [b_tmp mean(pdist2(Xtr(p,:),Xtr(notClusterMembersID,:),'cityblock'))];
                    otherwise
                        tmp = zeros(length(notClusterMembersID),1);
                        for q=1:length(tmp)
                            tmp(q) = obj.distance(Xtr(p,:),Xtr(notClusterMembersID(q),:));
                        end
                        b_tmp = [b_tmp mean(tmp)];
                end
                %                     b_tmp = [b_tmp mean(pdist2(Xtr(p,:),notClusterMembers,'mahalanobis',COVARIANCES{k}))];
            end
        end
        b(p)=min(b_tmp);

        s(p)=(b(p)-a(p))/max(a(p),b(p));
    end
    SILHOUETTE = mean(s);
    %     end
    fprintf('done!\t\t\t[Time elapsed: %.2f sec]\n\n',toc);
else
    SILHOUETTE = NaN; % NaN as a dummy value if user does not want Silhouette
end

%%% Output
outputArg = {FEASIBILITY,CENTROIDS,MEMBERSHIPS,HYPERPLANES,COVARIANCES,CARDINALITIES,DBI,SILHOUETTE,MAXANFISMF};
end
