function obj = fit(obj, X_TR, Y_TR, varargin)
import outstanding_library.MinMaxClassifier.*

% Get dimensions
numberOfLabels=length(Y_TR);
numberOfPattern=(size(X_TR,1));
numberOfVariables=size(X_TR,2);

MinMaxHome=mfilename('fullpath');
MinMaxHome = fileparts(MinMaxHome);

ID_TrFile = mat2str(clock);
trainingFile_ct4_fileName=strcat('dataraw_lambda_',ID_TrFile,'.ct4');
trainingFile_ct4_fullPath=fullfile(MinMaxHome,trainingFile_ct4_fileName);

obj.mat2ct4(X_TR, Y_TR, trainingFile_ct4_fullPath);
 
cmd = sprintf('"%s\\bin\\%s\\gparctrain.exe" -a %s -l %f -m "%s\\model_%s.txt" -t "%s"', ...
    MinMaxHome, obj.architecture, obj.trainingMode, obj.lambda, MinMaxHome, ID_TrFile, trainingFile_ct4_fullPath);
dos(cmd);

raw = {};
fid = fopen([MinMaxHome '\model_' ID_TrFile '.txt']);
tline = fgetl(fid);
raw=[raw {tline}];
while ischar(tline)
%     disp(tline)
    tline = fgetl(fid);
    raw=[raw {tline}];
end
fclose(fid);
raw=raw';

delete(trainingFile_ct4_fullPath);
delete([MinMaxHome '\model_' num2str(ID_TrFile) '.txt']);

numberOfHyperboxes= str2num(raw{find(cellfun(@(x) ~isempty(strfind(x,'HyperBoxes')),raw))+1});
find_hyperboxes=find(cellfun(@(x) ~isempty(strfind(x,'HyperBox_')),raw));

matrice_Hyperboxes=struct('min',[],'max',[],'label',[],'matriceDiRotazione',[] );

for iter_conversione=1:numberOfHyperboxes
    tmp_min=raw{find_hyperboxes(iter_conversione)+1};
    tmp_min = strsplit(tmp_min,'\t');
    tmp_min=cellfun(@(x) str2num(x),tmp_min);
    
    matrice_Hyperboxes(iter_conversione).min=tmp_min;
    
    tmp_max=raw{find_hyperboxes(iter_conversione)+2};
    tmp_max = strsplit(tmp_max,'\t');
    tmp_max=cellfun(@(x) str2num(x),tmp_max);
    
    matrice_Hyperboxes(iter_conversione).max=tmp_max;
    
    tmp_label=raw{find_hyperboxes(iter_conversione)+5};
    matrice_Hyperboxes(iter_conversione).label=str2num(tmp_label);
    
    tmp_matr_rotazione=eye(numberOfVariables,numberOfVariables);
    
    matrice_Hyperboxes(iter_conversione).matriceDiRotazione=tmp_matr_rotazione;
end

NUM_HYPERBOXES=size(matrice_Hyperboxes,2);
RETTANGOLI=cell(NUM_HYPERBOXES,2);
MATRICIROTAZIONE=cell(NUM_HYPERBOXES,1);
LABELS=cell(NUM_HYPERBOXES,1);

for iterHB=1:NUM_HYPERBOXES
    % Hyerbox
    RETTANGOLI{iterHB,1}= matrice_Hyperboxes(iterHB).min;
    RETTANGOLI{iterHB,2}= matrice_Hyperboxes(iterHB).max;
    
    % Rotating matrix
    MATRICIROTAZIONE(iterHB)={matrice_Hyperboxes(iterHB).matriceDiRotazione'};
    
    % Labels
    LABELS{iterHB} = matrice_Hyperboxes(iterHB).label;
    
    for iterPattern = 1:numberOfPattern
        nn = (sum(X_TR(iterPattern,:)<matrice_Hyperboxes(iterHB).max) + sum(X_TR(iterPattern,:)>matrice_Hyperboxes(iterHB).min)) == false; 
    end
    CARDINALITIES{iterHB} = sum(prod(X_TR<matrice_Hyperboxes(iterHB).max,2) & prod(X_TR>matrice_Hyperboxes(iterHB).min,2)); 
    
end  

obj.hyperboxes = RETTANGOLI;
obj.numHyperboxes = NUM_HYPERBOXES;
obj.rotatingMatrices = MATRICIROTAZIONE;
obj.labels = LABELS;


if obj.printFlag==true && numberOfVariables==2
    figure();
    Colore_HB=colormap(hsv(max (NUM_HYPERBOXES,numberOfLabels)));
    Colore_HB = Colore_HB(randperm(length(Colore_HB)),:);
    for iterLabelCluster=1:numberOfLabels
        printpattern=Y_TR==iterLabelCluster;
        if sum(printpattern)>0
            patternplot_X1=X_TR(printpattern,1);
            patternplot_X2=X_TR(printpattern,2);
            % STAMPA I PATTERN
            plot(patternplot_X1,patternplot_X2,'o','Color',Colore_HB(iterLabelCluster,:)); hold on
        end
    end
end

if obj.printFlag==true && numberOfVariables<=2
    for iterHB=1:NUM_HYPERBOXES
        rettangoloX1=[RETTANGOLI{iterHB,1}(1) RETTANGOLI{iterHB,1}(1)  RETTANGOLI{iterHB,2}(1)  RETTANGOLI{iterHB,2}(1)  RETTANGOLI{iterHB,1}(1) ];
        rettangoloX2=[RETTANGOLI{iterHB,1}(2) RETTANGOLI{iterHB,2}(2) RETTANGOLI{iterHB,2}(2) RETTANGOLI{iterHB,1}(2) RETTANGOLI{iterHB,1}(2)];
        % STAMPA RETTANGOLO RUOTATO
        hold on
        if CARDINALITIES{iterHB}>=numberOfVariables+1
            plot(rettangoloX1,rettangoloX2,'-','lineWidth',3,'Color',Colore_HB(obj.labels{iterHB},:)); hold on;axis('equal');
        end
    end
end
end