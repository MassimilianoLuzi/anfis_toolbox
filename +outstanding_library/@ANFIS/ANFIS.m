% Copyright (c) 2017, 2018 Stefano Leonori, Alessio Martino, Massimiliano Luzi
% University of Rome "La Sapienza"
%
% stefano.leonori@uniroma1.it, alessio.martino@uniroma1.it, massimiliano.luzi@uniroma1.it
%
% This file is part of ClassifierLibrary.
%
% ClassifierLibrary is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% HeuristicLibrary is distributed in the hope that it will be useful. 
% IT IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
% INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
% PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
% HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
% CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
% OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with ClassifierLibrary.If not, see<http://www.gnu.org/licenses/>.

classdef ANFIS
% Main class of the ANFIS classifier
    properties
        % ANFIS Output
        % Antecedent
        mu;
        C;
        % Consequent
        hyperplanes;
        ruleWeights;
        
        % Num Rules and num Clusters
        numRules;
        epsilon;
        
        % ANFISOptions
        ANFISOptions;
        
        % Training Options
        trainingOptions;
        trainedFlag;
        
        % Performance indeces
        DB_index;
        silhuette;
        
        % Callback Functions 
        CallbackPre;
        CallbackMid;
        CallbackPost;
        
        % Verbose
        verboseFlag;
    end
    
    methods
        % Constructor
        function obj = ANFIS(varargin)
			% ANFIS: Constructor of the ANFIS optimizer
			%
			% Input:
            % ANFISOptions: Options of the ANFIS system
			% trainingOptions: Options for the training algorithm
			% varargin: Supplementary options
			%
			% Output:
			% obj: ANFIS class object
			
			% Import library
            import outstanding_library.*
            
% 			% Handle for the minimal number of required arguments
%             if nargin <1
%                 error('The constructor must receive at leas one arguments: 1. The training options');
%             end
            
            % Default algorithm parameters
            default_ANFISOptions = struct(...       
                'defuzzification',      'WTA',      ... Defuzzification method. 'WTA' or 'AVG'. WTA: Winner Takes All: The ouput will be the hyperplane related to the highes memebrship function. AVG: Average: The output will be the average of all the hyperplanes weighted with the value of the related membership function 
                'normalizationFlag',    false,      ... Activate or deactivate the internal normalization of data of the woolbox
                'normalizationHeadroom',0.1         ...
            );
            
            default_trainingOptions = struct(...
                'parallelFlag',     true,           ... Set the activatino or the deactivatino of the parallel computing
                ... Clustering  
                'clusterMode',      'partitional',  ... Set the clustering mode: 'partitinoal', 'agg_hierarchical', 'div_hierarchical'
                'representative',   'centroid',     ... Set the representative type: 'centroid', 'medoid', 'median'
                'distance',         'sqEuclidean',  ... Set the distance to be used in clustering algorithm: 'euclidean', 'sqEuclidean', 'manhattan', 'mahalanobis' or function handle
                'numClusters',      1:10,           ... K candidates. Array ok K value to be tested. The best K will be kept.
                'numReplicates',    25,             ... Set the number of replications of the clustering algorithm. The clustering will be run for 'numReplicates' times with a different intialization. The best solution will be kept.
                'maxIterations',    1000,           ... Set the maximum number of itertions of the clusterin algorithm.
                'epsilon',          0.2,            ... Set the weight of the joint space clustering: 1: input space; 0: hyperplane clustering
                'davisBouldinFlag', false,          ... Set the evaluation or not of the Davis Bouldin index for the performance of the clustering algorithm 
                'silhoutteFlag',    false,          ... Set the evaluation or not of the Silhouette index for the performance of the clustering algorithm
                ... Min Max Classifier                  
                'minmaxFlag',       true,           ... Activation of the minmax classifier  for refining the clustering results
                'lambda',           0.2,            ... Tradeoff between accuracy and complexity for the minmax algorithm
                ... Print Options
                'printFlag',        false           ...
                );
            
            % Define the input parser
            p = inputParser;
            p.addParameter('trainingOptions', default_trainingOptions);
            p.addParameter('ANFISOptions', default_ANFISOptions);
            p.addParameter('CallbackPre', []);
            p.addParameter('CallbackMid', []);
            p.addParameter('CallbackPost', []);
            p.addParameter('Verbose', true);
           
            % Parse arguments
            p.parse(varargin{:});
            
            % Anfis options
            obj.ANFISOptions = p.Results.ANFISOptions;
            % Handle for undefined struct fields
            if ~isfield(obj.ANFISOptions, 'defuzzification')
                obj.ANFISOptions.defuzzification = 'WTA';
            end
            if ~isfield(obj.ANFISOptions, 'ruleWeights')
                obj.ANFISOptions.ruleWeights = [];
            end
            if ~isfield(obj.ANFISOptions, 'normalizationFlag')
                obj.ANFISOptions.normalizationFlag = false;
            end
            if ~isfield(obj.ANFISOptions, 'normalizationHeadroom')
                obj.ANFISOptions.normalizationHeadroom = 0.1;
            end
            
            % training options
            obj.trainingOptions = p.Results.trainingOptions;
            % Handle for undefined struct fields
            if ~isfield(obj.trainingOptions, 'parallelFlag')
                obj.trainingOptions.parallelFlag = false;
            end
            if ~isfield(obj.trainingOptions, 'clusterMode')
                obj.trainingOptions.clusterMode = 'partitional';
            end
            if ~isfield(obj.trainingOptions, 'representative')
                obj.trainingOptions.representative = 'centroid';
            end
            if ~isfield(obj.trainingOptions, 'distance')
                obj.trainingOptions.distance = 'sqEuclidean';
            end
            if ~isfield(obj.trainingOptions, 'numClusters')
                obj.trainingOptions.numClusters = 1:10;
            end
            if ~isfield(obj.trainingOptions, 'numReplicates')
                obj.trainingOptions.numReplicates = 25;
            end
            if ~isfield(obj.trainingOptions, 'maxIterations')
                obj.trainingOptions.maxIterations = 1000;
            end
            if ~isfield(obj.trainingOptions, 'epsilon')
                obj.trainingOptions.epsilon = 0.2;
            end
            if ~isfield(obj.trainingOptions, 'davisBouldinFlag')
                obj.trainingOptions.davisBouldinFlag = false;
            end
            if ~isfield(obj.trainingOptions, 'silhoutteFlag')
                obj.trainingOptions.silhoutteFlag = false;
            end
            if ~isfield(obj.trainingOptions, 'minmaxFlag')
                obj.trainingOptions.minmaxFlag = false;
            end
            if ~isfield(obj.trainingOptions, 'lambda')
                obj.trainingOptions.lambda = 0.2;
            end
            if ~isfield(obj.trainingOptions, 'printFlag')
                obj.trainingOptions.printFlag = false;
            end
            
            % Trainied Flag
            obj.trainedFlag = false;
            
            % Callback Options
            obj.CallbackPre = p.Results.CallbackPre;
            obj.CallbackMid = p.Results.CallbackMid;
            obj.CallbackPost = p.Results.CallbackPost;       
        end
    end
end