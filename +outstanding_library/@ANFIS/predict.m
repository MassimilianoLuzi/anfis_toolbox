function y = predict(obj, x)

numSamples = size(x,1);
numDimensions = size(x,2);
numMF = obj.numRules;

% Normalization
if obj.ANFISOptions.normalizationFlag && obj.trainedFlag
    x = obj.normalize(x, obj.ANFISOptions.normalizationX);
end

w = obj.ruleWeights;
y = zeros(numSamples,1);

% Fuzzification
MFval = zeros(numMF,1);
ruleVal=zeros(numMF,1);
for k=1:numSamples
    for iterMF=1:numMF
        gamma = 1e-7*eye(numDimensions);
        MFval(iterMF) = exp(-0.5*(x(k,:)-obj.mu{iterMF})*((obj.C{iterMF}+gamma)\(x(k,:)-obj.mu{iterMF})'));
        ruleVal(iterMF) = [1 x(k,:)]*obj.hyperplanes{iterMF};
    end
    
    % deFuzzification 
    switch obj.ANFISOptions.defuzzification
        case 'WTA'
            [~,winner]=max(MFval.*w);
            y(k) = ruleVal(winner);

        case 'AVG'
            y(k) = sum(MFval.*w.*ruleVal) / ( MFval'*w);   
            
        otherwise
            error('Wrong defuzzification mode. Available "WTA" or "AVG"');
    end

end

% DeNormalization
if obj.ANFISOptions.normalizationFlag && obj.trainedFlag
    y = obj.denormalize(y, obj.ANFISOptions.normalizationY);
end
end