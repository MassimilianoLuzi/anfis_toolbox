function [CENTROIDS_CLASSIFIER, HYPERPLANES_CLASSIFIER, COVARIANCE_CLASSIFIER, CARDINALITIES_CLASSIFIER, valid_NUM_HYPERBOXES]...
            = GetMembershipFromHyperboxes(obj, minmaxClassifier, X_TR, Y_TR)

numPatterns = size(X_TR, 1);
numVariables = size(X_TR, 2);

% Get minmax outputs
RETTANGOLI = minmaxClassifier.hyperboxes;
MATRICIROTAZIONE = minmaxClassifier.rotatingMatrices;
NUM_HYPERBOXES = minmaxClassifier.numHyperboxes;
HB_CLUSTER_ASSIGNMENT =  minmaxClassifier.labels;

%% CONTROLLO CORRETTO NUMER DI VARIABILI        
if numVariables ~= size(X_TR,2), error('ERRORE IN DEFINIZIONE PATTERN- DEFINIZIONE NUMERO DI VARIABILI');end
%    
RETTANGOLIPARALLELI=cell(NUM_HYPERBOXES,2);
for iter_HB=1: NUM_HYPERBOXES
    tmp_parallelizzazione=cellfun(@(x) x*MATRICIROTAZIONE{iter_HB} , RETTANGOLI(iter_HB,:), 'UniformOutput', false );
    RETTANGOLIPARALLELI(iter_HB,:)=[tmp_parallelizzazione(1),tmp_parallelizzazione(end)];
end

PATTERN_HB_MEMBERSHIP=zeros(numPatterns, NUM_HYPERBOXES); 

for iter_pattern=1:length(PATTERN_HB_MEMBERSHIP)
    tmp_elemento=X_TR(iter_pattern,:);
    for iter_HB=1: NUM_HYPERBOXES       
        tmp_elemento_antiruotato = tmp_elemento*MATRICIROTAZIONE{iter_HB};
        PATTERN_HB_MEMBERSHIP(iter_pattern,iter_HB)=(prod((tmp_elemento_antiruotato+.0)>=RETTANGOLIPARALLELI{iter_HB,1})*...
                                prod((tmp_elemento_antiruotato-.0)<=RETTANGOLIPARALLELI{iter_HB,2} ));
    end
end

CARDINALITA_HB = sum(PATTERN_HB_MEMBERSHIP);

valid_HyperboxesFlag = CARDINALITA_HB > numVariables+1;
valid_HYPERBOXES = RETTANGOLI(valid_HyperboxesFlag,:);
%valid_MATRICIROTAZIONE = MATRICIROTAZIONE(valid_HyperboxesFlag,:);
valid_HB_CLUSTER_ASSIGNMENT = HB_CLUSTER_ASSIGNMENT(valid_HyperboxesFlag, :);
valid_PATTERN_HB_MEMBERSHIP = PATTERN_HB_MEMBERSHIP(:,valid_HyperboxesFlag);
valid_NUM_HYPERBOXES = sum(valid_HyperboxesFlag);
valid_CARDINALITA_HB = CARDINALITA_HB(valid_HyperboxesFlag);

valid_PATTERN=cell(1,valid_NUM_HYPERBOXES);
valid_PATTERN_Y=cell(1,valid_NUM_HYPERBOXES);
CENTROIDS_CLASSIFIER=cell(1,valid_NUM_HYPERBOXES);
COVARIANCE_CLASSIFIER=cell(1,valid_NUM_HYPERBOXES);
HYPERPLANES_CLASSIFIER=cell(1,valid_NUM_HYPERBOXES);
CARDINALITIES_CLASSIFIER=cell(1,NUM_HYPERBOXES);
for iter_HB=1:valid_NUM_HYPERBOXES
    valid_PatternHBAssociation = valid_PATTERN_HB_MEMBERSHIP(:,iter_HB)==1;
    valid_PATTERN{1,iter_HB}= X_TR(valid_PatternHBAssociation,:);
    valid_PATTERN_Y{1,iter_HB}= Y_TR(valid_PatternHBAssociation,:);
    
    CENTROIDS_CLASSIFIER{1,iter_HB}=mean(valid_PATTERN{1,iter_HB},1);
    COVARIANCE_CLASSIFIER{1,iter_HB}=cov(valid_PATTERN{1,iter_HB});
    CARDINALITIES_CLASSIFIER{1, iter_HB} = valid_CARDINALITA_HB(iter_HB);
    HYPERPLANES_CLASSIFIER{iter_HB} = pinv([ones(size(valid_PATTERN{1,iter_HB},1),1) valid_PATTERN{1,iter_HB}]) * valid_PATTERN_Y{1,iter_HB};
%     
%     if CARDINALITIES_CLASSIFIER{1,iter_HB}+1<numVariables
%         HYPERPLANES_CLASSIFIER{iter_HB}=HYPERPLANES{ASSEGNAZIONE_HB_CLUSTER(iter_HB)};
%     else
%         HYPERPLANES_CLASSIFIER{iter_HB} = pinv([ones(size(PATTERN_CLASSIFIER{1,iter_HB},1),1) PATTERN_CLASSIFIER{1,iter_HB}]) * PATTERN_Y{1,iter_HB};
%     end
end


if obj.trainingOptions.printFlag && valid_NUM_HYPERBOXES~=0 && numVariables<=2
    figure();

    Colore_HB=colormap(colorcube(valid_NUM_HYPERBOXES));
    Colore_HB = Colore_HB(randperm(length(Colore_HB)),:);
    
    for iterHB=1:valid_NUM_HYPERBOXES
        printpattern=(valid_PATTERN_HB_MEMBERSHIP(:,iterHB))==1;
        if sum(printpattern)==0
            error('errore bug!')
        end
            patternplot_X1=X_TR(printpattern,1);
            patternplot_X2=X_TR(printpattern,2);
            % STAMPA I PATTERN
            plot(patternplot_X1,patternplot_X2,'o','Color',Colore_HB(valid_HB_CLUSTER_ASSIGNMENT{iterHB},:)); hold on
    end


    for iterHB=1:valid_NUM_HYPERBOXES
        rettangoloX1=[valid_HYPERBOXES{iterHB,1}(1) valid_HYPERBOXES{iterHB,1}(1)  valid_HYPERBOXES{iterHB,2}(1)  valid_HYPERBOXES{iterHB,2}(1)  valid_HYPERBOXES{iterHB,1}(1) ];
        rettangoloX2=[valid_HYPERBOXES{iterHB,1}(2) valid_HYPERBOXES{iterHB,2}(2) valid_HYPERBOXES{iterHB,2}(2) valid_HYPERBOXES{iterHB,1}(2) valid_HYPERBOXES{iterHB,1}(2)];
        centroidX1 = CENTROIDS_CLASSIFIER{iterHB}(1);
        centroidX2 = CENTROIDS_CLASSIFIER{iterHB}(2);
        % STAMPA RETTANGOLO RUOTATO
        hold on
        plot(rettangoloX1,rettangoloX2,'-','lineWidth',3,'Color',Colore_HB(valid_HB_CLUSTER_ASSIGNMENT{iterHB},:)); hold on;axis('equal');
        plot(centroidX1, centroidX2, 'p', 'MarkerFaceColor', Colore_HB(valid_HB_CLUSTER_ASSIGNMENT{iterHB},:) , 'MarkerEdgeColor', 'k', 'MarkerSize', 10);
    end

    % savefig(fig, 'hyperbox');
    % close(fig);
end

end

