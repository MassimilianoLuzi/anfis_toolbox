function obj = LoadFIS(obj, mu, C, hyperplanes, ruleWeights)
obj.mu = mu;
obj.C = C;
obj.hyperplanes = hyperplanes;
obj.ruleWeights = ruleWeights;
obj.numRules = length(mu);
end

