function loss = ValidationFcn(anfis, X_VL, Y_VL, validationOptions)

numPatterns  = size(X_VL, 1);
numFeatures  = size(X_VL, 2);

% Run ANFIS
Y_VL_predicted = anfis.predict(X_VL);

% Evaluate loss
loss = immse(Y_VL, Y_VL_predicted);

if strcmp(validationOptions, 'ValidationDataNotAvailable')
    alpha = 0.5;
    maxNumRules = floor(numPatterns/(numFeatures+1));
    loss = loss*alpha + anfis.numRules/maxNumRules*(1-alpha);
end

end