function obj = fit(obj, X_TR, Y_TR, varargin)

import outstanding_library.*

%% Parse the arguments

% Define the input parser
p = inputParser;
p.addRequired('X_TR');
p.addRequired('Y_TR');
p.addParameter('ValidationData', []);
p.addParameter('ValidationFcn', []);
p.addParameter('ValidationOptions', []);

% Parse arguments
p.parse(X_TR, Y_TR, varargin{:});

if obj.ANFISOptions.normalizationFlag
    [X_TR, normalizationX] = obj.normalize(X_TR, [], obj.ANFISOptions.normalizationHeadroom);
    [Y_TR, normalizationY] = obj.normalize(Y_TR, [], obj.ANFISOptions.normalizationHeadroom);
    obj.ANFISOptions.normalizationX = normalizationX;
    obj.ANFISOptions.normalizationY = normalizationY;
end
% Built Validation Set - If this is not provided, the validation is the
% Training Set itself
validationData = p.Results.ValidationData;
ValidationFcn = p.Results.ValidationFcn;
validationOptions = p.Results.ValidationOptions;

if isempty(validationData)
    X_VL = X_TR;
    Y_VL = Y_TR;
    if isempty(ValidationFcn)
        validationOptions = 'ValidationDataNotAvailable';
    end
else
    X_VL = p.Results.ValidationData{1};
    Y_VL = p.Results.ValidationData{2};
    if obj.ANFISOptions.normalizationFlag
        X_VL = obj.normalize(X_VL, normalizationX);
        Y_VL = obj.normalize(Y_VL, normalizationY);
    end
    
    if isempty(ValidationFcn)
        validationOptions = 'ValidationDataAvailable';
    end
end

%% Definisco Numero input ANFIS
numOfPattern = size(X_TR,1);
G_ANFIS_variables=size(X_TR,2);% size ANFIS input
    
fprintf('Running the clustering algorithm\n\n');

obj.trainedFlag = false;

%% Start Training
% Define best performance variable
best_rmsANFIS_VL = inf;
for epsilon = obj.trainingOptions.epsilon
    %%  APPLY CLUSTERING
    % Instantiate Clutering Algorithm
    clusteringAlgorithm = Clustering(...
        obj.trainingOptions.clusterMode,        ...
        obj.trainingOptions.representative,     ...
        obj.trainingOptions.distance,           ...
        obj.trainingOptions.numClusters,        ...
        obj.trainingOptions.numReplicates,      ...
        obj.trainingOptions.maxIterations,      ...
        epsilon,            ...
        obj.trainingOptions.davisBouldinFlag,   ...
        obj.trainingOptions.silhoutteFlag,      ...
        obj.trainingOptions.parallelFlag        ...
        );
    
    % Perform clustering
    clusteringAlgorithm = clusteringAlgorithm.fit(X_TR, Y_TR);
    
    % Get Results
    clust_feasibility   = clusteringAlgorithm.feasibility;
    clust_centroids     = clusteringAlgorithm.centroids;
    clust_memberships   = clusteringAlgorithm.memberships;
    clust_hyperplanes   = clusteringAlgorithm.hyperplanes;
    clust_covariances   = clusteringAlgorithm.covariances;
    clust_cardinalities = clusteringAlgorithm.cardinalities;
    clust_DBI           = clusteringAlgorithm.DBI;
    clust_silhouette    = clusteringAlgorithm.silhouette;
    
    
    %% ATTIVO PARFOR
    for k=obj.trainingOptions.numClusters
        
        %% Callback Pre
        % Run the CallbackPre at the beginning of the iterCluster
        if ~isempty(obj.CallbackPre)
            obj = obj.CallbackPre(obj, iter);
        end
        
        tmp_clust_NumberOfClusters = k;
        tmp_clust_feasibility = clust_feasibility(k,1:k);
        tmp_clust_centroids=clust_centroids(k,1:k);
        tmp_clust_memberships=clust_memberships(k,1:k);
        tmp_clust_hyperplanes=clust_hyperplanes(k,1:k);
        tmp_clust_covariances=clust_covariances(k,1:k);
        tmp_clust_cardinalities=clust_cardinalities(k,1:k);
        tmp_clust_DBI=clust_DBI(k);
        tmp_clust_silhouette=clust_silhouette(k);
        
        %% CONTROLLO FEASIBILITY
        if sum(cell2mat(tmp_clust_feasibility)==0)>0
            tmp_feasibilitySOL=0;
            fprintf('soluzione non accettata causa WCSOD, numOfClusters %d\n',tmp_clust_NumberOfClusters)
        elseif (sum(cellfun(@length,tmp_clust_memberships))-numOfPattern==0)==false
            tmp_feasibilitySOL=0;
            fprintf('soluzione non accettata causa bug DIV, numOfClusters %d\n',tmp_clust_NumberOfClusters)
        else
            tmp_feasibilitySOL=1;
        end
        
        %% Callback Mid
        % Run the CallbackMid between the clustering and the classifier phase
        if ~isempty(obj.CallbackMid)
            obj = obj.CallbackMid(obj, iter);
        end
        
        %% ESEGUO AGORITMO DI CLASSIFICAZIONE SE LA SOLUZIONE � FEASIBLE
        if tmp_feasibilitySOL && obj.trainingOptions.minmaxFlag
            
            %% ESEGUO ALGORITMO DI CLASSIFICAZIONE
            minmaxClassifier = MinMaxClassifier('lambda', obj.trainingOptions.lambda, 'print', obj.trainingOptions.printFlag);
            
            % Get pattern labels by means of cluster ids
            numberOfLabels=length(tmp_clust_memberships);
            
            labeledPatterns=[];
            for iter_labels=1:numberOfLabels
                numPatternsInCluster = length(tmp_clust_memberships{iter_labels});
                labeledPatterns = vertcat(labeledPatterns, [tmp_clust_memberships{iter_labels}, iter_labels*ones(numPatternsInCluster,1)]);
            end
            labeledPatterns=sortrows(labeledPatterns,1);
            labels_TR = labeledPatterns(:,2);
            
            % Run MinMax
            minmaxClassifier = minmaxClassifier.fit(X_TR, labels_TR);
            
            fprintf('NUMERO DI CLUSTER = %d \n NUMERO DI HB = %d \n',k,minmaxClassifier.numHyperboxes);
            
            %% SPEZZO E RIDEFINISCO I CLUSTERS CON I RETTANGOLI RUOTATI DELL'ALGORITMO DI CLASSIFICAZIONE
            [class_CENTROIDS,class_HYPERPLANES,class_COVARIANCE,class_CARDINALITIES,class_numHyperboxes] = ...
                obj.GetMembershipFromHyperboxes(minmaxClassifier, X_TR, Y_TR);
            
            %% ASSEGNO PARAMETRI ANFIS
            tmp_centroids=class_CENTROIDS;
            tmp_hyperplanes=class_HYPERPLANES;
            tmp_covariances=class_COVARIANCE;
            tmp_cardinalities=class_CARDINALITIES;
            
            if length(tmp_centroids)<=1
                tmp_feasibilitySOL=false;
                fprintf('candidato eliminato perche non ci sono HB\n')
            end
            
            fprintf('NUMERO DI HB VALIDI = %d \n',class_numHyperboxes);
            
        else
            %% ASSEGNO PARAMETRI ANFIS SENZA SUPPORTO DELL'ALGORITMO DI CLASSIFICAZIONE
            tmp_centroids=tmp_clust_centroids;
            tmp_hyperplanes=tmp_clust_hyperplanes;
            tmp_covariances=tmp_clust_covariances;
            tmp_cardinalities=tmp_clust_cardinalities;
        end
        
        if tmp_feasibilitySOL==true
            
            tmp_DBIk=tmp_clust_DBI;
            tmp_silhuettek=tmp_clust_silhouette;
            
            %% DEFINISCO L'ANFIS
            tmp_ruleWeights=ones(length(tmp_centroids), 1);
            obj = obj.LoadFIS(tmp_centroids, tmp_covariances, tmp_hyperplanes, tmp_ruleWeights);
            
            %% CALCOLO L'ANFIS VL RMSE
            if isempty(ValidationFcn)
                tmp_rmsANFIS_VL= obj.ValidationFcn(X_VL, Y_VL, validationOptions);
            else
                tmp_rmsANFIS_VL= ValidationFcn(obj, X_VL, Y_VL, validationOptions);
            end
            
            %% Check if the best configuration has been improved
            if tmp_rmsANFIS_VL < best_rmsANFIS_VL
                best_rmsANFIS_VL = tmp_rmsANFIS_VL;
                best_centroids = tmp_centroids;
                best_covariances = tmp_covariances;
                best_hyperplanes = tmp_hyperplanes;
                best_ruleWeights = tmp_ruleWeights;
                best_epsilon = epsilon;
            end
        end
        %% Callback Post
        % Run the CallbackPost at the beginning of the iterCluster
        if ~isempty(obj.CallbackPost)
            obj = obj.CallbackPost(obj, iter);
        end
    end
end

%% Save best ANFIS configuration
if exist('best_centroids', 'var')
    obj.trainedFlag = true;
    obj = obj.LoadFIS(best_centroids, best_covariances, best_hyperplanes, best_ruleWeights);
    obj.epsilon = best_epsilon;
else
    obj.trainedFlag = false;
    error('The Training has failed in finding a suitable ANFIS');
end


end
