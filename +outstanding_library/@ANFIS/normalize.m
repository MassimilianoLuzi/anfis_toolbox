function [X_norm, normalizationCoefficients] = normalize(obj, X, normalizationCoefficients, headroom)

if nargin == 3
    X_min = normalizationCoefficients(1,:);
    X_max = normalizationCoefficients(2,:);
elseif nargin==4
    X_min = min(X);
    X_max = max(X);

    delta = headroom*(X_max-X_min);
    X_min = X_min - delta/2;
    X_max = X_max + delta/2;
    
    normalizationCoefficients = [X_min; X_max];
end

X_norm = (X-X_min)./(X_max-X_min);
end