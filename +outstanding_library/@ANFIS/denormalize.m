function X = denormalize(obj, X_norm, normalizationCoef)
X_min = normalizationCoef(1,:);
X_max = normalizationCoef(2,:);

X = X_min + X_norm.*(X_max-X_min);
end